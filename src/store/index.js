import Vue from "vue";
import Vuex from "vuex";
import province from "./province";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    province: province,
    member: [
      {
        level: "ทั่วไป",
        id: 1,
      },
      {
        level: "Gold",
        id: 2,
      },
      {
        level: "Silver",
        id: 3,
      },
    ],
    cardLevel: [
      {
        label: "Silver Card",
        value: "Silver Card",
      },
      {
        label: "Gold Card",
        value: "Gold Card",
      },
      {
        label: "Premium Card",
        value: "Premium Card",
      },
    ],
  },
  getters: {},
  mutations: {},
  actions: {},
  modules: {},
});
