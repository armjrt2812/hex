import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "./assets/tailwind.css";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "./styles/index.scss";
import "./styles/element-variables.scss";
import "@/mixin/auth";

import locale from "element-ui/lib/locale";
import lang from "element-ui/lib/locale/lang/en";

Vue.config.productionTip = false;
locale.use(lang);
Vue.use(ElementUI);
new Vue({
  router,
  store,
  render: function (h) {
    return h(App);
  },
}).$mount("#app");
