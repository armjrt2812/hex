import { HTTP } from "@/service/axios";

export default {
  loginAPI(obj, token) {
    return new Promise((resolve) => {
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      HTTP.post(`/signin`, obj, config)
        .then((response) => {
          const result = response.data.result;
          return resolve(result);
        })
        .catch((error) => {
          return reject(error.response.data);
        });
    });
  },
};
