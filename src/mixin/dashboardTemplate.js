import DashboardTemplate from "@/template/DashboardTemplate";
import Vue from "vue";

Vue.mixin({
  components: {
    DashboardTemplate,
  },
});
