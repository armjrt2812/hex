import Vue from 'vue'
import { HTTP } from '@/service/axios'

Vue.mixin( {
    computed: {
        user ()
        {
            return  this.$store.state.user
        },
        token ()
        {
            return this.$store.state.token
        }
    }
})