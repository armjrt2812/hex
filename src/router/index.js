import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/sigin-in",
  },
  {
    path: "/sigin-in",
    name: "Sign in",
    component: function () {
      return import("@/views/Login");
    },
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: function () {
      return import("@/views/Dashboard");
    },
  },
  {
    path: "/member",
    name: "Member",
    component: function () {
      return import("@/views/member/Member");
    },
    children: [
      {
        path: "",
        name: "Member list",
        component: function () {
          return import("@/views/member/MemberList");
        },
      },
      {
        path: "list",
        name: "Member list",
        component: function () {
          return import("@/views/member/MemberList");
        },
      },
      {
        path: "detail/:id",
        name: "Member Detail",
        component: function () {
          return import("@/views/member/MemberDetail");
        },
      },
      {
        path: "edit/:id",
        name: "Member Edit",
        component: function () {
          return import("@/views/member/EditMember");
        },
      },
      {
        path: "patient/:id",
        name: "Patient Edit",
        component: function () {
          return import("@/views/member/EditPatient");
        },
      },
      {
        path: "card/:id",
        name: "Card Edit",
        component: function () {
          return import("@/views/member/MemberCard");
        },
      },
    ],
  },
  {
    path: "/nurse",
    name: "Nurse",
    component: function () {
      return import("@/views/Nurse");
    },
    children: [
      {
        path: "",
        name: "nurse list",
        component: function () {
          return import("@/views/nurse/List");
        },
      },
      {
        path: "list",
        name: "nurse list",
        component: function () {
          return import("@/views/nurse/List");
        },
      },
      {
        path: "detail/:id",
        name: "Nurse Detail",
        component: function () {
          return import("@/views/nurse/Detail");
        },
      },
      {
        path: "edit/:id",
        name: "Nurse Edit",
        component: function () {
          return import("@/views/nurse/Edit");
        },
      },
      {
        path: "card/:id",
        name: "Nurse Card Edit",
        component: function () {
          return import("@/views/nurse/Card");
        },
      },
      {
        path: "certificate",
        name: "Certificate List",
        component: function () {
          return import("@/views/nurse/Certificate");
        },
      },
    ],
  },
  {
    path: "/finance",
    name: "Finance",
    component: function () {
      return import("@/views/Finance");
    },
    children: [
      {
        path: "",
        name: "finance list",
        component: function () {
          return import("@/views/finance/List");
        },
      },
      {
        path: "list",
        name: "finance list",
        component: function () {
          return import("@/views/finance/List");
        },
      },
      {
        path: "detail/:id",
        name: "Finance Detail",
        component: function () {
          return import("@/views/finance/Detail");
        },
      },
    ],
  },
  {
    path: "/ads",
    name: "Ads",
    component: function () {
      return import("@/views/Ads");
    },
  },
  {
    path: "/report",
    name: "Report",
    component: function () {
      return import("@/views/Report");
    },
  },
  {
    path: "/admin",
    name: "Admin",
    component: function () {
      return import("@/views/Admin");
    },
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
